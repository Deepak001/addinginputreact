import React, { Component } from 'react';
import { DataConsumer } from '../App';
class Person extends Component {
//    static contextType=DataConsumer;
state={
    name1:'in'
}
    componentDidMount() {
        console.log("hii",this.props);
        const { name,age } = this.props.context.state;
        this.props.context.inc();
        this.props.context.nameChange('raj');
        // this.setState({name1:name});
        console.log(name);
    }
    componentDidUpdate(prevProps, prevState) {
        // Previous ThemeContext value is prevProps.theme
        // New ThemeContext value is this.props.theme
        // const { name,age } = this.props.context.state;
        // console.log("didUpdate",prevProps.context,this.props.context,"===",prevState);
      }
  render() {
    const { context } = this.props;
      return (
          <div>
              <p>{this.state.name1}</p>
               
              <p>hey , this is a Person..{context.state.name}</p>
              <p>hey , this is age.{context.state.age}</p>
                    <button onClick={context.inc}>+</button>
          </div>
      )
  }  
}
// Person.contextType=DataConsumer;

export default props => ( 
   <DataConsumer.Consumer>
    {(context) => {
       return <Person {...props} context={context} />
    }}
  </DataConsumer.Consumer>
)