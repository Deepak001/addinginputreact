import React from 'react'


class DocumentInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          newItem: "",
          itemList: []
        };
      }

    componentDidMount() {
        this.hydrateStateWithLocalStorage();
    
        // add event listener to save state to localStorage
        // when user leaves/refreshes the page
        window.addEventListener(
          "beforeunload",
          this.saveStateToLocalStorage.bind(this)
        );
      }
    
    componentWillUnmount() {
        window.removeEventListener(
          "beforeunload",
          this.saveStateToLocalStorage.bind(this)
        );
    
        // saves if component has a chance to unmount
        this.saveStateToLocalStorage();
      }
    
      hydrateStateWithLocalStorage() {
        // for all items in state
        for (let key in this.state) {
          // if the key exists in localStorage
          if (localStorage.hasOwnProperty(key)) {
            // get the key's value from localStorage
            let value = localStorage.getItem(key);
    
            // parse the localStorage string and setState
            try {
              value = JSON.parse(value);
              this.setState({ [key]: value });
            } catch (e) {
              // handle empty string
              this.setState({ [key]: value });
            }
          }
        }
    }
    
    saveStateToLocalStorage() {
        // for every item in React state
        for (let key in this.state) {
          // save to localStorage
          localStorage.setItem(key, JSON.stringify(this.state[key]));
        }
      }
    

    updateInput(key, value) {
        // update react state
        this.setState({ [key]: value });
      }
    saveItem() {
            // create a new item
      const newItem = {
        id: 1 + Math.random(),
        value: this.state.newItem.slice()
       };
  
      // copy current list of items
      const list = [...this.state.itemList];
  
      // add the new item to the list
      list.push(newItem);
  
      // update state with new list, reset the new item input
      this.setState({itemList:list});
    }

    deleteItem() {
            //some
            console.log(this.state);
            localStorage.clear();
        }
 render() {
      return (
      <div>
            <input 
                type="text" 
                name='name'
                placeholder='enter text'
                value={this.state.newItem} 
                onChange={e => this.updateInput("newItem", e.target.value)}
            />

            <button onClick={() => this.saveItem()}>
                Save
            </button>
            <button onClick={() => this.deleteItem()}>
                Delete
            </button>
       </div>     
      );
    }
  }


 export default DocumentInput; 