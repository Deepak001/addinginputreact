import React, { Component } from 'react';
import AddItem from './components/addItem'
import './App.css';
import DocumentInput from './components/documentInput'
import Family from './components/family'
import Modal from './components/model'
// import Person from './components/person'
const MyContext =React.createContext();
console.log("hhh",MyContext);
export const DataConsumer =MyContext;
class MyProvider extends Component {
  state ={
    name:'dev',
      age:23
  }
  render() {
    return (
      <MyContext.Provider value={{state:this.state,
      inc: () => this.setState({age: this.state.age+1}),
      nameChange: (newName) => this.setState({name:newName})}}>
        {this.props.children}
      </MyContext.Provider>
    )
  }
}


class App extends Component {
  constructor(props){
    super(props);
     this.state = { 
      documents: [],
      name:'',
      showModal: false
    }
    this.handleShow = this.handleShow.bind(this);
    this.handleHide = this.handleHide.bind(this);
    this.add = this.add.bind(this);
    this.remove = this.remove.bind(this);
  }

  add() {
    const documents = this.state.documents.concat(DocumentInput);
    this.setState({ documents });
  }
  remove() {
    const list = [...this.state.documents];
    //console.log(list.length)
     list.splice(0,1);
    console.log(list.length)
    this.setState({documents:list});
     
  }
  handleShow() {
    this.setState({showModal: true});
  }
  
  handleHide() {
    this.setState({showModal: false});
  }
  componentDidMount() {
    document.addEventListener('keyup',this.keyup,false)
  }
  componentWillUnmount() {
    document.addEventListener('keyup',this.keyup,false)
  }
  keyup =e => {
    if (e.key==='Escape') {
      this.handleHide();
    }
  }
 // handleClick(e) {
//   e.stopPropagation();
//   alert("modal clicked!");
// }
handleClick2() {
  // alert(" clicked!");
  this.setState({showModal: false});
}
  render() {
    console.log("state",this.state);
    const documents = this.state.documents.map((Element, index) => {
      return <Element key={ index } />
    });

    const modal = this.state.showModal ? (
      <Modal>
        <div className="modal" onClick={this.handleClick}>
          <div>
            With a portal, we can render content into a different
            part of the DOM, as if it were any other React child.
          </div>
          This is being rendered inside the #modal-container div.
          <button onClick={this.handleHide}>Hide modal</button>
        </div>
      </Modal>
    ) : null;
    return (
    <MyProvider>
      <div className="App" onClick={this.handleClick2.bind(this)} >
       <div style={{width:'auto'}}>
         <button onClick={this.remove} style={{float:'left',marginLeft:10}} >-</button>
          <div className='one'>
          {/* <AddItem /> */}
           {documents}
            </div>
            <button onClick={this.add}>+</button>
        </div>

       <div className='two'>
       {/* new box */}
          <Family />
        
       </div>
      
      </div>
      <button style={{color:'red',marginTop:'50%',marginLeft:'50%',cursor:'pointer',userSelect:'none'}} onClick={this.handleShow}>Show modal</button>
        {modal}
      </MyProvider>
    );
  }
}

export default App;
